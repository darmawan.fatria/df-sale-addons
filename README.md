# Deskripsi 
Addons untuk handle biaya tambahan seperti Service fee, Management fee dll.

# Konfigurasi

## Membuat Jenis Fee
- Menu : Sales -> Configuration -> Order Fee Type

* Fee : Jumlah biaya tambahan untuk transaksi sales order
* Product : default produk yang akan otomatis di create pada order dan invoice line
* Taxes : Jika jenis fee ini dikenakan tax
* Description : default deskripsi yang akan muncul pada order line, jika null maka akan di set jadi  name nya

## Menentukan produk yang terkena additional fee
- Menu : Sales->Products->Product variants
- Pada mode Add/Edit pilih tab Sales , maka check Will **Charge For Any Additional Fee?** jika akan di hitung sebagai biaya tambahan

## Konfigurasi Customers
- Menu : Sales -> Sales -> Customers. 
- Pada Form view di tab Sales & Purchase  : Pilih jenis fee
- Untuk role manager dapat merubah nilai fee (tidak berdasar pada konfigurasi default)


## Sales order
- Pada saat memilih customer, additional fee akan otomatis terisi berdasar konfigurasi di menu customer.
- Custom Fee hanya akan muncul dan bisa diubah oleh manager.
- Proses kalkulasi akan terjadi saat menekan tombol confirm sale. Dan otomatis akan menambah line di order line.


## Account invoice (Updated 07-Des-2016)
- Create invoce, Pada saat memilih customer perilaku nya sama dengan ketika membuat sales order
- Sebelum validate invoice, proses kalkulasi (sementara) dilakukan dengan menekan tombol **Generate Fee**. Hal ini dilakukan untuk membedakan perhitungan kalkulasi account invoice manual, dengan account invoice yang berasal dari sale order
- Tax akan digenerate setelah klik Validate (sesuai dengan bawaan default odoo)

