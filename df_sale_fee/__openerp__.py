##############################################################################
#
#    Darmawan Fatriananda [darmawan.fatria@gmail.com]
#    November 2016
##############################################################################

{
    "name": "Perhitungan Service Fee",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Sale",
    "description": """
    Perhitung service fee
    """,
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "base",
                "sale",
                "product",
                "account"
                ],
    'data': [
                "sale_view.xml",
                "res_partner_view.xml",
                "product_view.xml",
                "sale_fee_type_view.xml",
                "account_invoice_view.xml",
                "security/ir.model.access.csv",
            ],
    
}
