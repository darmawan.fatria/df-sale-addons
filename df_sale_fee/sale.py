##############################################################################
#
#    Darmawan Fatriananda [darmawan.fatria@gmail.com]
#    November 2016
##############################################################################

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class sale_order(osv.Model):
    _inherit = 'sale.order'

    _columns = {
        'sale_fee_type_id' : fields.many2one('sale.fee.type', 'Fee Type',
                                             required=False,
                                             readonly=True, states={'draft': [('readonly', False)]},),
        'fee': fields.float('Fee',  digits_compute=dp.get_precision('Product Price'),
                                             readonly=True, states={'draft': [('readonly', False)]},),
        'fee_calculation_type': fields.selection([('percent', 'Percent'), ('amount', 'Amount')],
                                             readonly=True, states={'draft': [('readonly', False)]}
                                                 ,string='Fee Calculation Type'),
        'fee_total': fields.float('Total Fee', readonly=True, states={'draft': [('readonly', False)]},
                                  digits_compute=dp.get_precision('Product Price'), ),

        #'sale_order_fee_line_ids': fields.one2many('sale.order.fee.line', 'order_id', 'Additionall Fee Lines', readonly=True,
        #                              states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        #                              ),

    }
    _defaults = {
        'fee_calculation_type': 'percent',
        'fee_total': 0.0,
    }

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        #print "onchange_partner_id : ",part
        parent_result = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=None)
        if part:
            partner = self.pool.get('res.partner').browse(cr, uid, part, context=context)
            if partner.sale_fee_type_id:
                sale_fee_type_id = partner.sale_fee_type_id.id
                custom_partner_fee = partner.sale_fee_type_id.fee
                if partner.fee > 0:
                    custom_partner_fee = partner.fee
                order_fee = custom_partner_fee
                parent_result['value']['sale_fee_type_id'] = sale_fee_type_id
                parent_result['value']['fee']= order_fee
        #print parent_result
        return parent_result
    #generate fee
    def generate_additional_fee(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        for _order in self.browse(cr, uid, ids, context=context):
            if _order.sale_fee_type_id :
                price_total_is_add_fee = 0.0  #
                for _line in _order.order_line:
                    if _line.product_id.is_additional_fee :
                        price_total_is_add_fee = price_total_is_add_fee +_line.price_subtotal
                if _order.fee_calculation_type == 'percent':
                    fee_total = float((_order.fee * price_total_is_add_fee)/100)
                else :
                    fee_total = _order.fee
                #print fee_total
                self.write(cr,uid,_order.id,{'fee_total': fee_total},context=context)
                res_parent =  self._amount_all_wrapper(cr,uid,[_order.id],field_name=None,arg=None,context=None);
                self.write(cr, uid, _order.id, {
                     'amount_untaxed': res_parent[_order.id]['amount_tax'],
                    'amount_tax': res_parent[_order.id]['amount_untaxed'] + fee_total,
                    'amount_total': (res_parent[_order.id]['amount_untaxed'] + _order.fee_total) + res_parent[_order.id]['amount_tax'] ,
                }, context=context)

    def action_wait(self, cr, uid, ids, context=None):
        line_pool = self.pool.get('sale.order.line')
        self.generate_additional_fee(cr,uid,ids,context=context)
        for _order in self.browse(cr, uid, ids, context=context):
            if _order.sale_fee_type_id:
                #print " _order.fee_total ",_order.fee_total
                fee_tax_ids = False
                if _order.sale_fee_type_id.tax_id:
                    fee_tax_ids = _order.sale_fee_type_id.tax_id
                order_line_vals = {
                    'order_id' : _order.id,
                    'name' : _order.sale_fee_type_id.description or _order.sale_fee_type_id.name,
                    'price_unit' : _order.fee_total,
                    'product_uom_qty' : 1,
                    'sequence':999,
                    'product_id': _order.sale_fee_type_id.product_id and _order.sale_fee_type_id.product_id.id or False,
                    'tax_id': fee_tax_ids and [(6, 0, [x.id for x in fee_tax_ids])],
                }
                new_line_id = line_pool.create(cr, uid, order_line_vals, context=context)


        return super(sale_order, self).action_wait(cr, uid, ids,  context=context)

sale_order()

#class sale_order_fee_line(osv.Model):
#    _name = 'sale.order.fee.line'
#sale_order_fee_line()