##############################################################################
#
#    Darmawan Fatriananda [darmawan.fatria@gmail.com]
#    November 2016
##############################################################################

from openerp.osv import fields, osv
from openerp import  api, _
import openerp.addons.decimal_precision as dp


class account_invoice(osv.Model):
    _inherit = 'account.invoice'

    _columns = {
        'sale_fee_type_id': fields.many2one('sale.fee.type', 'Fee Type',
                                            required=False,
                                            readonly=True, states={'draft': [('readonly', False)]}, ),
        'fee': fields.float('Fee', digits_compute=dp.get_precision('Product Price'),
                            readonly=True, states={'draft': [('readonly', False)]}, ),
        'fee_calculation_type': fields.selection([('percent', 'Percent'), ('amount', 'Amount')],
                                                 readonly=True, states={'draft': [('readonly', False)]}
                                                 , string='Fee Calculation Type'),
        'fee_total': fields.float('Total Fee', readonly=True, states={'draft': [('readonly', False)]},
                                  digits_compute=dp.get_precision('Product Price'), ),
    }
    _defaults = {
        'fee_calculation_type': 'percent',
        'fee_total': 0.0,
    }

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
            payment_term=False, partner_bank_id=False, company_id=False):
        parent_result = super(account_invoice, self).onchange_partner_id(type, partner_id, date_invoice, payment_term, partner_bank_id,company_id)
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)
            if partner.sale_fee_type_id:
                sale_fee_type_id = partner.sale_fee_type_id.id
                custom_partner_fee = partner.sale_fee_type_id.fee
                if partner.fee > 0:
                    custom_partner_fee = partner.fee
                order_fee = custom_partner_fee
                parent_result['value']['sale_fee_type_id'] = sale_fee_type_id
                parent_result['value']['fee'] = order_fee
        return parent_result

    #recreate _compute_amount
    @api.v7
    def compute_in_v8(self, cr, uid, invoice_id, context=None):
        #print "compute in v8 or v7"
        res = {}
        recs = self.browse(cr, uid, [], context)
        invoice = recs.env['account.invoice'].browse(invoice_id)
        recs._compute_amount(invoice)
        #print recs
        res[invoice.id] = {
            'amount_untaxed': invoice.amount_untaxed,
            'amount_tax': invoice.amount_tax,
            'amount_total': invoice.amount_total,
        }
        return res
    #generate fee
    @api.v7
    def generate_additional_fee(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        for _invoice in self.browse(cr, uid, ids, context=context):
            if _invoice.sale_fee_type_id :
                price_total_is_add_fee = 0.0  #
                for _line in _invoice.invoice_line:
                    if _line.product_id.is_additional_fee :
                        price_total_is_add_fee = price_total_is_add_fee +_line.price_subtotal
                if _invoice.fee_calculation_type == 'percent':
                    fee_total = float((_invoice.fee * price_total_is_add_fee)/100)
                else :
                    fee_total = _invoice.fee
                #print fee_total
                self.write(cr,uid,_invoice.id,{'fee_total': fee_total},context=context)
                res_parent =  self.compute_in_v8(cr,uid,_invoice.id,context=None);
                #print "res_parent  ",res_parent
                self.write(cr, uid, _invoice.id, {
                    'amount_untaxed': res_parent[_invoice.id]['amount_tax'],
                    'amount_tax': res_parent[_invoice.id]['amount_untaxed'] + fee_total,
                    'amount_total': (res_parent[_invoice.id]['amount_untaxed'] + _invoice.fee_total) + res_parent[_invoice.id]['amount_tax'] ,
                }, context=context)

    @api.v7
    def action_do_generate_fee(self, cr, uid, ids, context=None):
        line_pool = self.pool.get('account.invoice.line')
        self.generate_additional_fee(cr, uid, ids, context=context)
        for _invoice in self.browse(cr, uid, ids, context=context):
            if _invoice.sale_fee_type_id:
                #print " _order.fee_total ", _invoice.fee_total
                fee_tax_ids = False
                if _invoice.sale_fee_type_id.tax_id:
                    fee_tax_ids = _invoice.sale_fee_type_id.tax_id
                order_line_vals = {
                    'invoice_id': _invoice.id,
                    'name': _invoice.sale_fee_type_id.description or _invoice.sale_fee_type_id.name,
                    'price_unit': _invoice.fee_total,
                    'sequence': 999,
                    'product_id': _invoice.sale_fee_type_id.product_id and _invoice.sale_fee_type_id.product_id.id or False,
                    'invoice_line_tax_id': fee_tax_ids and [(6, 0, [x.id for x in fee_tax_ids])],
                    'partner_id' : _invoice.partner_id.id,
                    #'account_id':None,
                    #'uos_id':None,
                    'quantity':1,
                }
                #print " ---> add new line <--- "
                new_line_id = line_pool.create(cr, uid, order_line_vals, context=context)
        return True

account_invoice()