##############################################################################
#
#    Darmawan Fatriananda [darmawan.fatria@gmail.com]
#    November 2016
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime, timedelta
import time


class sale_fee_type(osv.Model):
    _name = 'sale.fee.type'
    _description = 'Konfigurasi Fee'

    _columns = {
        'name'       : fields.char('Name', required=True, size=25),
        'fee'        : fields.float('Fee', required=True,),
        'description': fields.text('Description',),

        'tax_id': fields.many2many('account.tax', 'sale_fee_type_tax', 'sale_fee_type_id', 'tax_id', 'Taxes',
                                   ),
        'product_id': fields.many2one('product.product', 'Product', domain=[('sale_ok', '=', True)],
                                      ),

    }


sale_fee_type()