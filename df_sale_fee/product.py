##############################################################################
#
#    Darmawan Fatriananda [darmawan.fatria@gmail.com]
#    November 2016
##############################################################################
from openerp.osv import fields, osv



class product_product(osv.Model):
    _inherit = 'product.product'

    _columns = {
        'is_additional_fee': fields.boolean('Will Charge For Any Additional Fee?'),
    }


product_product()
