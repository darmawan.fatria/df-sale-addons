##############################################################################
#
#    Darmawan Fatriananda [darmawan.fatria@gmail.com]
#    November 2016
##############################################################################
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class res_partner(osv.Model):
    _inherit = 'res.partner'

    _columns = {
        'sale_fee_type_id': fields.many2one('sale.fee.type', 'Fee Type',
                                            required=False, ),
        'fee': fields.float('Custom Fee', digits_compute=dp.get_precision('Product Price')),
    }


res_partner()
